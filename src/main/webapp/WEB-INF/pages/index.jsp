<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>信息管理系统</title>
</head>
<style>
    .logindiv{
        width: 400px;
        height: 400px;
        position: relative;
        left: 40%;
        top: 25%;
        font-size: 35px;
        border: 2px solid cadetblue;
        border-radius: 50px 50px 50px 50px;
        background-color: lightblue;

    }
    .login_name_pwd{
        width: 400px;
        height: 250px;
    }
    .hname{
        text-align: center;
    }
    .div_name{
        width: 400px;
        height: 80px;
    }
    .div_pwd{
        width: 400px;
        height: 80px;
    }
    .div_bt {
        padding-left:18%;
    }
    .input_name{
        width: 200px;
        height: 50px;
        font-size: 20px;
        border:1px solid lightblue;
        border-radius: 50px 50px 50px 50px;
    }
    .input_pwd{
        width: 200px;
        height: 50px;
        font-size: 20px;
        border:1px solid lightblue;
        border-radius: 50px 50px 50px 50px;
    }
    .bt_login{
        width: 120px;
        height: 40px;
        font-size: 20px;
        border:1px solid lightblue;
        border-radius: 50px 50px 50px 50px;
        background-color: dodgerblue;
    }
    .bt_return{
        width: 120px;
        height: 40px;
        font-size: 20px;
        position: relative;
        top: -74px;
        left: 212px;
        border:1px solid lightblue;
        border-radius: 50px 50px 50px 50px;
        background-color: dodgerblue;
    }
</style>
<body>
<div class="logindiv">
    <div class="hname">
        <h4 class="manage-title" >信息管理系统</h4>
    </div>
    <div class="login_name_pwd">
        <form moth="get" id="loginform" action="${pageContext.request.contextPath}/usrlogin">
            <div class="div_name">
                <span style="padding-left: 10% " font-size="20px">账号：</span>
                <input class="input_name" type="text" id="usrname" name="usrname" placeholder="请输入用户账号">
            </div>
            <div class="div_pwd">
                <span style="padding-left: 10%">密码：</span>
                <input class="input_pwd" type="text" id="usrpwd" name="usrpwd" placeholder="请输入用户密码">
            </div>
            <div class="div_bt">
                <input class="bt_login" type ="submit" value="登录"/>
            </div>
        </form>
        <a href="${pageContext.request.contextPath}/quxiao">
            <input class="bt_return" type ="submit" value="取消" />
        </a>
    </div>
</div>
</body>
</html>