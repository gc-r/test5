<%--
  Created by IntelliJ IDEA.
  User: Lenovo
  Date: 2024/3/25
  Time: 23:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>信息管理系统</title>
    <style>
        .fail{
            font-size: 30px;
        }
    </style>
</head>
<body>
    <div class="fail">
        <h4 class="manage-title" >登录失败，请返回</h4>
        <a href="${pageContext.request.contextPath}/return">返回登录</a>
    </div>
</body>
</html>
