<%--
  Created by IntelliJ IDEA.
  User: Lenovo
  Date: 2024/3/25
  Time: 23:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>信息管理系统</title>
    <style>
        .usrdata{
            width: 400px;
            height: 521px;
            position: relative;
            left: 40%;
            top: 25%;
            border: 2px solid cadetblue;
            border-radius: 50px 50px 50px 50px;
            background-color: lightblue;
        }
        .data_title{
            text-align: center;
            height: 25px;
        }
    </style>
</head>
<body>
<div class="usrdata">
    <div class="data_title">
        <h4 class="manage-title" style="font-size: 30px">用户信息</h4>
    </div>
    <div class="data" style="padding-left: 10%;font-size: 20px">
        <h4>用户名：${name1}</h4>
        <h4>专业：物联网工程</h4>
        <h4>年级：2021级</h4>
        <h4>班级：21-01班</h4>
        <h4>学号：2021443562</h4>
        <h4>联系方式：12345678910</h4>
        <h4>家庭住址：重庆市沙坪坝区</h4>
        <h4>籍贯：广西壮族自治区</h4>
        <h4>   </h4>
    </div>
</div>
</body>
</html>
















