package com.example.controller;

import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.model.SimpleCounter;


@Controller
public class BaseController {

	private static final String VIEW_INDEX = "index";

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String back() {
		return "index";
	}

	@RequestMapping(value = "/usrlogin",method = RequestMethod.GET)
	public String usrlogin(String usrname, String usrpwd, Model model) {
		if(usrname.equals("hsy") && usrpwd.equals("2021443562")){
			model.addAttribute("name1",usrname);
			return "logincg";
		}
		return "loginfail";
	}
	@RequestMapping(value = "/quxiao",method = RequestMethod.GET)
	public String quxiao(String usrname, String usrpwd) {
		return "index";
	}

	@RequestMapping(value = "/{name}", method = RequestMethod.GET)
	public String welcomeName(@PathVariable String name, ModelMap model) {
		return VIEW_INDEX;
	}

}
